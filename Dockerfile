#https://nodejs.org/en/docs/guides/nodejs-docker-webapp/  use commands from this url
FROM node:16.13.1
# Create app directory
WORKDIR /usr/src/app
COPY package*.json ./
COPY .prod.env ./.env
COPY index.js ./
RUN npm install

EXPOSE 3000

CMD ["node", "index"]
